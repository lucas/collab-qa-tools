old = IO::read('buildtime.list').split("\n").map { |l| l.split }
oldh = Hash[*old.flatten]
new = IO::read('times').split("\n").map { |l| l = l.split ; [l[0], l[3].split('.')[0] ] }
newh = Hash[*new.flatten]
(oldh.keys + newh.keys).uniq.each do |k|
  if newh[k]
    puts "#{k} #{newh[k]}"
  else
    puts "#{k} #{oldh[k]}"
  end
end
